# PROIEL to AGLDT

This repository contains the code for a converter for Latin texts from the PROIEL annotation scheme to the AGLDT annotation scheme.

## Relavent links

PROIEL release repository: https://github.com/syntacticus/syntacticus-treebank-data\
PROIEL syntactic guidelines: http://dev.syntacticus.org/proiel-annotators-handbook-v1.pdf\
PROIEL visualization tool: https://syntacticus.org/browse\
AGLDT guidelines for Latin: https://github.com/PerseusDL/treebank_data/tree/master/v1/latin/docs\
AGLDT https://perseusdl.github.io/treebank_data/\

# Funder

<a href="http://www.dfg.de/index.jsp" target="_blank">
<img src="https://upload.wikimedia.org/wikipedia/commons/8/86/DFG-logo-blau.svg" 
width="" height="40" alt=""/>
</a>

(Project number: 408121292)

# Licence

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons License" style="border-width:0" 
src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br/>
This work is licensed under a <a rel="license" 
href="http://creativecommons.org/licenses/by-sa/4.0/">
Creative Commons Attribution-ShareAlike 4.0 International License</a>.
