#!/usr/bin/env python
# coding: utf-8

# In[1]:


import xml.etree.ElementTree as ET
import pandas as pd
from sklearn.model_selection import train_test_split
import numpy as np
from xml.dom import minidom
import time
import os


# In[8]:


def transform_xml(xml_doc):


    for sentence in root.iter('sentence'):

        for token in sentence:
            token.set('SID', sentence.get('id'))
            attr = token.attrib
            my_dict = attr.copy()
            #my_dict.update(token.attrib)


            yield my_dict


# In[33]:


def turn_xml_to_df(path):
    
    # Parse the XML file


    trans = transform_xml(root)
    df = pd.DataFrame(list(trans))

    #I Split the data into a training and a test dataframe
    #I also make sure that the dataframes are only divided by sentence
    df_train, df_test = train_test_split(df['SID'].unique(),test_size=0.1, random_state=42,shuffle = False)

    df_train = df[df['SID'].isin(df_train)]
    df_test = df[df['SID'].isin(df_test)]
    df = df_train.copy()
    
    return df,df_test


# In[10]:


#I strip a column of all nans

def strip_col(x):
    if x is not np.nan:
        return x.strip()


# In[11]:


#Creates new tokens for punctuation
def punct_to_token(df):
    new_rows = []
    index_iam_at = 0
    
    roots = df.set_index('head-id').loc[np.nan]
    
    for index, row in df[['presentation-before','presentation-after','citation-part', 'id', 'SID','head-id']].iterrows():
        punct = str(row['presentation-after'])
        punct_bef = str(row['presentation-before'])
        #Safes variable of the first token without a head-id -> root

        #Creates new row for punctuation after
        if punct not in 'Nonenan':
          SID = row['SID']
          head_id = row['id']

            
          
          if punct == '.' or punct == ';' or punct ==':':
              head_id = roots[roots['SID']== row['SID']]['id'].values[0]

          new_row = {
              'id': np.nan,
              'form': punct,
              'citation-part':row['citation-part'],
              'lemma': punct,
              'part-of-speech': 'Pu',
              'morphology': np.nan,
              'head-id': head_id,
              'relation': 'punkt',
              'information-status': np.nan,
              'presentation-after': np.nan,
              'SID': SID,
              'antecedent-id': np.nan,
              'contrast-group': np.nan,
              'empty-token-sort': np.nan,
              'presentation-before': np.nan
          }

          #if statement garantees that if there are presentation-after instances
          #with more than one character they get safed as two seperater token
          if len(punct) == 2:

              new_row1 = new_row.copy()
              new_row1['form'] = punct[0]

              new_row2 = new_row.copy()
              new_row2['form'] = punct[1]

              new_rows.append((index, new_row1))
              new_rows.append((index, new_row2))

              continue

          new_rows.append((index, new_row))

        #Creates new token for presentation-before
        if punct_bef not in 'Nonenan':
          SID = row['SID']
          head_id = row['id']

          new_row = {
              'id': np.nan,
              'form': punct_bef,
              'citation-part':row['citation-part'],
              'lemma': punct_bef,
              'part-of-speech': 'Pu',
              'morphology': np.nan,
              'head-id': head_id,
              'relation': 'punkt',
              'information-status': np.nan,
              'presentation-after': np.nan,
              'SID': SID,
              'antecedent-id': np.nan,
              'contrast-group': np.nan,
              'empty-token-sort': np.nan,
              'presentation-before': np.nan
          }


          new_rows.append((index-1, new_row))



    new_df = pd.DataFrame([row for idx, row in new_rows])


    df['original_index'] = df.index


    new_df['original_index'] = [idx for idx, row in new_rows]


    combined_df = pd.concat([df, new_df]).sort_values(by=['original_index','id'],na_position = 'last').reset_index(drop=True)
    combined_df.drop(columns=['original_index'], inplace=True)

    return combined_df


# In[12]:


#Number through all tokens in a sentence defining new id and starting at one
def new_index(df):
    df['SID'] = df['SID'].astype(int)
    l=1
    for i in df['SID'].drop_duplicates():
        df_slice = df[df['SID'] == i]
    #     print(df_slice['SID'])
    #     df_slice = df_slice.reset_index()
    #     print(df_slice['SID'])
        
        j=1
        k=0
        
        for index,tok in df_slice.iterrows():

            df.loc[index,'NewSID'] = l 
            df.loc[index,'NewID'] = j

            j+=1
            
            if not(pd.isna(tok["empty-token-sort"])):
                df.loc[index,'form'] = f"[{k}]"
                df.loc[index,'lemma'] = f"[{k}]"
                
                k+=1
                
        l+=1

    df['NewID'] = df['NewID'].astype(int)
    df['NewSID'] = df['NewSID'].astype(int)


    


# In[13]:


#Creates new 'postag' column for the new annotation scheme
pos_dict = {'A-':'a','Df':'d','S-':'-','Ma':'m','Nb':'n','C-':'c','Pd':'p','F-':'-','Px':'p','N-':'-',
            'I-':'i','Du':'d','Pi':'p','Mo':'m','Pp':'p','Pk':'p','Ps':'p','Pt':'p','R-':'r','Ne':'n',
            'Py':'-','Pc':'p','Dq':'d','Pr':'p','G-':'c','V-':'v','X-':'-','Pu':'u'}

def postag_create(row):
    postag = list('---------')
    if str(row['part-of-speech']) != 'nan':

        postag[0] = pos_dict[row['part-of-speech']]
        
    if not pd.isna(row['empty-token-sort']):
        postag[0] = row['empty-token-sort'].lower()
        
    if type(row['morphology']) == type('str'):
        morph = list(row['morphology'])

        if 'x' not in morph[0:2] and 'd' not in morph[0:2]:
            postag[1:3] = morph[0:2]


        if morph[2] in 'pirltf':
            postag[3] = morph[2]

        if morph[3] in 'isnmpdgu':
            postag[4] = morph[3]

        if morph[4] in 'ap':
            postag[5] = morph[4]

        postag[6] = morph[5]

        if morph[6] in 'ngdabvl':
            postag[7] = morph[6]

        if morph[7] in 'cs':
            postag[8] = morph[7]

    return ''.join(postag)



        
        
    
    




# In[14]:


#Enables one to create a new "order column" to get tokens in the right order

def set_order_vals(row):
    
    if pd.notna(row['empty-token-sort']):
        return 1
    
    if row['form'] == '.' or row['form'] == ';':
    
        return 2
    
    else:
        return 0


# In[15]:


#Orders tokens by the "order column"

def order_tokens(df_apply):

    df_apply['order_col'] = np.nan

    df_apply['order_col'] = df_apply.apply(lambda row: set_order_vals(row), axis=1)

    return df_apply.sort_values(by=['SID','order_col'])
    


# In[16]:


#Create new head by a new system

def new_head(row):
    
    
    if pd.notna(row['head-id']):
        return id_to_newid.get(row['head-id'])
    
    else:
        return 0
    
  


# In[17]:


#Drops all unreferenced empty token

def drop_unreferenced_tokens(df_apply):
    head_list = df_apply['head-id'].to_list()
    drop_indices = df_apply[(df_apply['id'].isin(head_list) == False) 
                          & (df_apply['empty-token-sort'].isna() == False)].index
    
    return df_apply.drop(drop_indices)


# In[18]:


def relation_translation(row):
    # Translation of all relation which do not need special treatment
    rel_dict = {'PRED': 'OBJ', 'VOC': 'ExD', 'SUB': 'SBJ', 'OBJ': 'OBJ', 'OBL': 'OBJ', 'AG': 'OBJ',
                'ADV': 'ADV', 'XOBJ': 'PNOM', 'XADV': 'ADV',
                'ARG': 'OBJ', 'PER': 'OBJ', 'NONSUB': 'OBJ',
                'ATR': 'ATR', 'APOS': 'APOS', 'PART': 'ATR', 'NARG': 'OBJ',
                'AUX': 'AuxY', 'PUNKT': 'P', 'PARPRED': 'ExD', 'COMP': 'COMP', 'REL': 'unkown', 'ADNOM': 'unknown'}

    relation = rel_dict[row['relation'].upper()]

    # Special treatment if "PRED" is the root of a sentence
    if (row['newhead'] == 0) and (row['relation'].upper() == 'PRED'):
        return 'PRED'

    # Preposition
    if row['part-of-speech'] == 'R-':
        return 'AuxP'
    # Transform tokens depending on Preposition
    if row['head-id'] in id_to_pos.keys() and id_to_pos[row['head-id']] == 'R-':
        return rel_dict[id_to_rel[row['head-id']].upper()]

    # xobj
    if row['relation'] == 'xobj':
        try:
            for i in df_apply1.set_index('head-id').loc[row['head-id']]['relation'].values:

                if rel_dict[i.upper()] == 'OBJ':
                    return 'OCOMP'

                else:
                    pass

        except:
            pass

    # APOS
    if row['relation'].upper() == 'APOS' and row['part-of-speech'] == 'V-':
        return 'ATR'

    # Differentiate different kinds for Aux
    if row['relation'].upper() == 'AUX' and row['part-of-speech'] == 'V-':
        return 'AuxV'

    if row['relation'].upper() == 'AUX' and row['form'] == 'non':
        return 'AuxZ'

    # Special treatment of Comp and related tokens
    if row['relation'].upper() == 'COMP':

        if row['postag'][0] == 'c':
            return 'AuxC'

        elif row['postag'][0] == 'v':
            return 'OBJ'

    if row['id'] in id_to_postag.keys() and row['head-id'] in id_to_postag.keys():
        if id_to_postag[row['id']][0] == 'c' and id_to_rel[row['head-id']].upper() == 'COMP':
            return 'OBJ'

        if row['form'] == ',':
            return 'AuxX'
        # Should ':' also have relation 'AuxK'?
        if row['form'] == '.' or row['form'] == ';' or row['form'] == ':':
            return 'AuxK'
    # Regular translation
    return rel_dict[row['relation'].upper()]


# In[19]:


#Funtion for translating all "parpred" relations
def parpred_treatment(df_apply):
    for i in df_apply['SID'].unique():

        if 'PRED' in df_apply1.set_index(['SID', 'new_rel']).loc[i].index.unique():
            for index, row in df_apply[df_apply['SID'] == i][
                ['id', 'SID', 'head-id', 'relation', 'part-of-speech', 'NewID', 'newhead', 'new_rel',
                 'empty-token-sort']].iterrows():

                if row['relation'] == 'parpred':
                    try:
                        closest_pred_id = row['NewID'] - min(
                            abs(int(row['NewID']) - df_apply.set_index(['SID', 'new_rel']).loc[i].loc['PRED', 'NewID']))

                        df_apply.loc[index, 'newhead'] = closest_pred_id
                    except:

                        pred_id = df_apply.set_index(['SID', 'new_rel']).loc[i].loc['PRED', 'NewID']

                        df_apply.loc[index, 'newhead'] = pred_id
        else:
            continue
        



# In[131]:


dicta = {'a':2,'b':5,'c':7}

dicta.pop('a')

dicta


# In[148]:


#Function for translating all "conjunction" type tokens

def conjuction_treatment_new(df_apply):
    con_id = {}
    subtag = []
    
    for i in df_apply['SID'].unique():
        sentence_marker = 0
        
        side_dict = {}
        coord_id = {}
        for index, row in df_apply[df_apply['SID'] == i][['id','form', 'SID','head-id','relation','part-of-speech','NewID','newhead','new_rel','empty-token-sort','postag']].iterrows():
            
            current_id = row['NewID']
            
            if (sentence_marker == 1) and row['newhead'] == 0:

                df_apply.loc[index,'newhead'] = conhead
                df_apply.loc[index,'new_rel'] = row['new_rel']+'_CO'
                sentence_marker = 0
                
            if row['postag'][0] == 'c' and row['relation'] != 'comp':
                
                if row['part-of-speech'] == 'G-':
                    con_id[row['id']] = row['new_rel']
                    subtag.append(row['id'])
                    df_apply.loc[index,'new_rel'] = 'AuxC'
                    
                     
                    coord_id[f'COORD{current_id}'] = [row['id'],row['form'],row['id']]
                    
                    continue
                
                if row['NewID'] == 1:
                    conhead = row['NewID']
                    df_apply.loc[index,'newhead'] = 0
                    df_apply.loc[index,'new_rel'] = 'AuxY'
                    coord_id[f'COORD{current_id}'] = [row['id'],row['form'],row['id']]
                    sentence_marker = 1
                    
                
                else:
                    con_id[row['id']] = row['new_rel']
                    df_apply.loc[index,'new_rel'] = 'AuxY'
                    coord_id[f'COORD{current_id}'] = [row['id'],row['form'],row['id']]
                    
                    
                
                

                
                
                if row['head-id'] in con_id.keys():
                    con_id[row['id']] = con_id[row['head-id']]
                
                
                safe_for_later = 0
                for i in coord_id.keys():
                    
                    
                        
                    if (row['form'] == coord_id[i][1]) and (row['head-id'] == coord_id[i][2]):
                            
                            coord_id[i][0] = coord_id[i][2]
                            coord_id.pop(f'COORD{current_id}')
                            if safe_for_later !=0:
                                 coord_id.pop(safe_for_later)
                            break
                            
                    if (row['head-id'] == coord_id[i][0]):
                        safe_for_later = i
                        coord_id[i][0] = row['id']
                        
                    else:
                        pass
                safe_for_later = 0
                
          
        for i in coord_id.keys():
            
            if df_apply.loc[df_apply[df_apply['id'] == coord_id[i][0]].index[0],'part-of-speech'] == 'G-':
                pass
            else:
                df_apply.loc[df_apply[df_apply['id'] == coord_id[i][0]].index[0],'new_rel'] = 'COORD'
                
            
            
    return con_id,subtag


# In[106]:


#additional function for the conjunction_treatment function, handles subordinate conjunctions

def name_conjunction_subordinate(row,con_id,subtag):
    
        
    if row['head-id'] in con_id.keys():
        if row['postag'][0] == 'c':
            
            return row['new_rel']
        
        if row['head-id'] in subtag:
            return con_id[row['head-id']]
            
        return con_id[row['head-id']]+'_CO'
        
    else:
        return row['new_rel']


# In[88]:


#Translating the finished dataframe back to xml-file


# In[89]:


def choose_format(val):
    
    if val:
        return df_to_XML_new_format
    
    else:
        return df_to_XML_vis_form_new_attr


# In[90]:


def df_to_XML_new_format(df,file_name):

    tree = ET.parse('phi0975.phi001.perseus-lat1.tb.xml')
    #Repository: 'data/target_annotation_scheme/perseus_target_annot_reference.xml'
    #Home Laptop: 'phi0975.phi001.perseus-lat1.tb.xml'
    root = tree.getroot()                                 

    body = root[1]
    body.clear()

    for i,j in zip(df['SID'].unique(),df['NewSID'].unique()):
        sentence = ET.SubElement(body, 'sentence')
        sentence.set('old_id',str(i))
        sentence.set('new_id',str(j))


    col_list = ['id','NewID','form','citation-part','lemma','head-id','relation','presentation-after',
                'presentation-before','postag','empty-token-sort']



    for sent in root.iter('sentence') :

        for index, row in df[df['SID'] == int(sent.get('old_id'))].iterrows():


            token = ET.SubElement(sent, 'token')
            attributes = row.to_dict()
            for attr in df.columns:
                
                if str(attributes[attr]) in "Nonenan":
                    continue
                token.set(str(attr), str(attributes[attr]))
                
                
    xml_string = ET.tostring(root, encoding='unicode')
    xml_string_cleaned = xml_string.replace('  ','').replace('\n','').replace('   ','')
    
    parsed_string = minidom.parseString(xml_string_cleaned)
    pretty_string = parsed_string.toprettyxml(indent="  ", encoding='utf-8')
    
    with open(file_name, "wb") as f:
        f.write(pretty_string)
    


# In[91]:


def df_to_XML_vis_form_new_attr(df,file_name):

    tree = ET.parse('treebank (3).xml')
    #Repository: 'data/target_annotation_scheme/perseus_target_annot_reference.xml'
    #Home Laptop: 'phi0975.phi001.perseus-lat1.tb.xml'
    #Visual Format: 'treebank (3).xml'
    root = tree.getroot()
    
    attributes = root.attrib
    root.clear()
    root.attrib.update(attributes)
#     body = root[1]
#     body.clear()

    
    for i,j in zip(df['SID'].unique(),df['NewSID'].unique()):
        sentence = ET.SubElement(root, 'sentence')
        #sentence.set('old_id',str(i))
        sentence.set('id',str(j))


    col_list = ['NewID','form','lemma','postag','new_rel','newhead']
    name_list = ['id','form','lemma','postag','relation','head']



    for sent in root.iter('sentence') :

        for index, row in df[df['NewSID'] == int(sent.get('id'))].iterrows():


            token = ET.SubElement(sent, 'word')
            attributes = row.to_dict()
            
            for i in range(len(col_list)):
                
                if str(attributes[col_list[i]]) == "None" or str(attributes[col_list[i]]) == "nan":
                    continue
                token.set(name_list[i], str(attributes[col_list[i]]))
                
                
    xml_string = ET.tostring(root, encoding='unicode')
    xml_string_cleaned = xml_string.replace('  ','').replace('\n','').replace('   ','')
    
    parsed_string = minidom.parseString(xml_string_cleaned)
    pretty_string = parsed_string.toprettyxml(indent="  ", encoding='utf-8')
    
    with open('visual_'+file_name, "wb") as f:
        f.write(pretty_string)


# In[125]:


parent_dir = input('Enter Folder directory: ')
value = input('Perseid format (y/n): ')

if value == 'y':
    value = False
else:
    value = True

item = 'caes-gal.xml'
    
for item in os.listdir(parent_dir):
    file_path = os.path.join(parent_dir, item)

    tree = ET.parse(file_path) #Repository:'data/proiel/caes-gal.xml' 
    #Home Laptop:'caes-gal.xml'
    root = tree.getroot()
    df,df_test = turn_xml_to_df(file_path)

    #Run through all functions defined so far: Creating tokens for punctuation, creating new ID per senetence,
    #creating new Postag column which translates from previous column
    df_apply = df.copy()

    start = time.perf_counter()



    #Drop unreferenced tokens
    df_apply = drop_unreferenced_tokens(df_apply)
    # Preparation for creating new tokens
    #-> strips presentation after/before columns of all empty spaces
    df_apply['presentation-after'] = df_apply['presentation-after'].apply(strip_col)
    df_apply['presentation-before'] = df_apply['presentation-before'].apply(strip_col)

    df_apply = punct_to_token(df_apply)


    df_apply['postag'] = df_apply.apply(postag_create,axis=1)


    df_apply['SID'] = df_apply['SID'].astype(int)
    df_apply = order_tokens(df_apply)
    df_apply.drop(columns = ['order_col'],inplace = True)
    df_apply.reset_index(inplace = True,drop = True)


    new_index(df_apply)


    id_to_newid = df_apply.set_index('id')['NewID'].to_dict()

    df_apply['newhead'] = df_apply.apply(lambda row: new_head(row), axis=1)
    df_apply['newhead'] = df_apply['newhead'].astype(int)

    df_apply1 = df_apply.copy()

    id_to_pos = df_apply.set_index('id')['part-of-speech'].to_dict()
    id_to_postag = df_apply.set_index('id')['postag'].to_dict()
    id_to_rel = df_apply.set_index('id')['relation'].to_dict()


    df_apply1['new_rel'] = df_apply1.apply(lambda row: relation_translation(row),axis = 1)

    parpred_treatment(df_apply1)

    con_id,subtag = conjuction_treatment_new(df_apply1)

    df_apply1['new_rel'] = df_apply1.apply(lambda row: name_conjunction_subordinate(row,con_id,subtag),axis = 1)






    df_apply = df_apply1.copy()

    format_funct = choose_format(value)

    format_funct(df_apply,'trans_'+item)

    end = time.perf_counter()
    print(item,'processed, time: ',end-start,'s')
print('\n All Done.')


# In[ ]:





# In[ ]:




