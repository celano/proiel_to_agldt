import os
import glob
import random
from lxml import etree

random.seed(342776)

def prettyprint(sentences, path):
    root = etree.Element("treebank")
    root.extend(sentences)
    xml = etree.tostring(root, pretty_print=True)
    with open(path, "w") as f:
      print(xml.decode(), end='', file=f)

dir_files = "../data/proiel"
dir_files_norm = os.path.normpath(dir_files)
glob_path = os.path.join(dir_files_norm, "*.xml")
files = glob.glob(glob_path)

all_xml = []
for x in files:
  tree = etree.parse(x)
  sentences = tree.xpath(".//sentence")
  all_xml.extend(sentences)

random.shuffle(all_xml) 

t9 = round(len(all_xml) * 0.90)

train = all_xml[0: t9]
test = all_xml[t9 :]

prettyprint(train, os.path.join(os.path.dirname(dir_files), "train.xml"))
prettyprint(test, os.path.join(os.path.dirname(dir_files), "test.xml"))
